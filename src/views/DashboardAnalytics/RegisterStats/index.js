import React, { useState, useEffect, useRef } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Card, CardHeader, CardContent, Divider
} from '@material-ui/core';
import GenericMoreButton from 'src/components/GenericMoreButton';
import CanvasJSReact from './canvasjs.react';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {},
  content: {},
  buttons: {
    display: 'flex',
    justifyContent: 'center',
    '& > *': {
      marginLeft: theme.spacing(1)
    }
  },
  inner: {
    height: 375,
    minWidth: 500
  },
  chart: {
    height: '100%'
  }
}));

function RegisterStats(props) {
const classes = useStyles();;
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

let datas = useRef({
  animationEnabled: true,	
				title:{
					text: ""
				},
				axisY : {
					title: "Number of Sign-ups",
					includeZero: false
				},
				toolTip: {
					shared: true
				},
				data: [{
					type: "spline",
					name: "Bookipi",
					showInLegend: true,
					dataPoints: []
				},
				{
					type: "spline",
					name: "Desktop",
					showInLegend: true,
					dataPoints: []
        },
        {
					type: "spline",
					name: "Super Simple App",
					showInLegend: true,
					dataPoints: []
        },
        {
					type: "spline",
					name: "Invoice Bee",
					showInLegend: true,
					dataPoints: []
        },
        {
					type: "spline",
					name: "Bookipi Expense",
					showInLegend: true,
					dataPoints: []
        },
      ]
});

useEffect(() => {

  const token = service.getToken();
  const key = service.getKey();

  const setter = (i,p,d) => {
    datas.current.data[i].dataPoints.push({y : p.count, label: d._id.year + "/" + d._id.month + "/" + d._id.day}); 
    // console.log(datas.current.data[i].dataPoints)
  }
  axios
  .get(`${API_ENDPOINT}/v1/gangster/dashboard/${JSON.stringify(props.startDate)}/${JSON.stringify(props.endDate)}`, { params: { access_token: token, x_key: key }  })
  .then( res => {
    res.data.monthData.map(d => {
      d.platform.map(p => {
        if(p.platform == 'b'){setter(0,p,d);}
        if(p.platform == 'd'){setter(1,p,d);}
         if(p.platform == 's'){setter(2,p,d);}
         if(p.platform == 'i'){setter(3,p,d);}
         if(p.platform == 'e'){setter(4,p,d);}
      })
    })
  })
}, []);

  return (
    <Card
      // {...rest}
      className={clsx(classes.root)}
    >
      <CardHeader
        action={<GenericMoreButton />}
        title="Statistics"
      />
      <Divider />
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
         {console.log(datas.current)}
          <CanvasJSChart options = {datas.current}/>
          </div>
        </PerfectScrollbar>
      </CardContent>
    </Card>
  );
}

// RegisterStats.propTypes = {
//   className: PropTypes.string
// };

export default RegisterStats;
