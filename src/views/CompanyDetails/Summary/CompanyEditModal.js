import React, { useState } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Grid,
  Divider,
  Typography,
  TextField,
  Switch,
  Button
} from '@material-ui/core';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';
import SuccessSnackbar from '../../Settings/General/SuccessSnackbar';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    boxShadow: theme.shadows[20],
    width: 700,
    maxHeight: '100%',
    overflowY: 'auto',
    maxWidth: '100%'
  },
  actions: {
    justifyContent: 'flex-end'
  }
}));

function CompanyEditModal({
  open, onClose, company, className, ...rest
}) {
  const classes = useStyles();
  const [values, setValues] = useState({
    ...company
  });
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const handleFieldChange = (event) => {
    event.persist();
    setValues((currentValues) => ({
      ...currentValues,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? event.target.checked
          : event.target.value
    }));
  };


  const handleSnackbarClose = () => {
    setOpenSnackbar(false);
  };

    const handleSubmit = (event) => {
      event.preventDefault();

      const token = service.getToken();
      const key = service.getKey();
      const user = service.getUser();

      const company = {
        c: values.c,
        e: values.e,
        co: values.co,
        tz: values.tz,
        st: values.st,
        access_token: token,
        x_key: key
      }
      axios
      .put(`${API_ENDPOINT}/v1/company/${company._id}/update`, company)
      .then(response => {
        console.log(`${company.c} is updated`)
        setOpenSnackbar(true);
      })
      .catch(error => {console.log(error)})
    }

  if (!open) {
    return null;
  }

  return (
    <Modal
      onClose={onClose}
      open={open}
    >
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <form>
          <CardHeader title="Edit Company" />
          <Divider />
          <CardContent>
            <Grid
              container
              spacing={3}
            >
                <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label="Company Name"
                  name="c"
                  onChange={handleFieldChange}
                  value={values.c}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label="Company Email"
                  name="e"
                  onChange={handleFieldChange}
                  value={values.e}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label="Location"
                  name="co"
                  onChange={handleFieldChange}
                  value={values.co}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label="City"
                  name="tz"
                  onChange={handleFieldChange}
                  value={values.tz}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label="State"
                  name="st"
                  onChange={handleFieldChange}
                  value={values.st}
                  variant="outlined"
                />
              </Grid>
              <Grid item />
              <Grid
                item
                md={6}
                xs={12}
              >
                <Typography variant="h5">Email Verified</Typography>
                <Typography variant="body2">
                  Disabling this will automatically send the company a verification
                  email
                </Typography>
                <Switch
                  checked={values.verified}
                  color="secondary"
                  edge="start"
                  name="verified"
                  onChange={handleFieldChange}
                  value={values.verified}
                />
              </Grid>
            </Grid>
          </CardContent>
          <Divider />
          <CardActions className={classes.actions}>
            <Button onClick={onClose}>
              Close
            </Button>
            <Button
              color="primary"
              onClick={handleSubmit}
              variant="contained"
            >
              Save
            </Button>
          </CardActions>
        </form>
        <SuccessSnackbar
        onClose={handleSnackbarClose}
        open={openSnackbar}
      />
      </Card>
    </Modal>
  );
}

CompanyEditModal.propTypes = {
  className: PropTypes.string,
  company: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

CompanyEditModal.defaultProps = {
  open: false,
  onClose: () => {}
};

export default CompanyEditModal;
