import axios from 'axios'
import { API_ENDPOINT } from "../config/config";

const baseUrl = `${API_ENDPOINT}`

let bookipiToken = null 
let bookipiKey = null

const setBookipiToken = (newToken, newKey) => {
      bookipiToken = newToken;
      bookipiKey = newKey;
  }

const postData = async (newObject, path) => {

  const config = {
    headers: { 
        "Accept" : "application/json",
        "Content-Type": "application/json",
        "X-Access-Token": bookipiToken,
        "x-Key" : bookipiKey
    }
}
  const response = await axios.post(`${baseUrl}${path}`, newObject, config)
  console.log(response.data)
  return response.data
}

const getToken = () => {
const bookipiToken =  window.localStorage.getItem('@BkpInvoice:_token');
var token;
if(bookipiToken){
  token = JSON.parse(bookipiToken);
}
return token;
}

const getKey = () => {
  const bookipiKey =   window.localStorage.getItem('@BkpInvoice:_key');
  var key;
  if(bookipiKey){
     key = JSON.parse(bookipiKey);
  }
  return key;
}

const getUser = () => {
  const bookipiUser =   window.localStorage.getItem('loggedUser');
  var user;
  if(bookipiUser){
     user = JSON.parse(bookipiUser);
  }
  return user;
}

export default { setBookipiToken , postData , getToken, getKey, getUser}