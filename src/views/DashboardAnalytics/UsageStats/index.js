import React, { useEffect, useState, useRef } from 'react';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import { Card, CardHeader, CardContent, Divider } from '@material-ui/core';
import GenericMoreButton  from 'src/components/GenericMoreButton';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';
import axios from 'axios';
import CanvasJSReact from './canvasjs.react';
 
const useStyles = makeStyles(theme => ({
  root: {
    height: '100%'
  },
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  chart: {
    padding: theme.spacing(4, 2, 0, 2),
    height: 400
  }
}));

const UsageStats = (props) => {
  const classes = useStyles();
  var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
  let data = useRef({
    exportEnabled: true,
    animationEnabled: true,
    title: {
      text: "Website Traffic Sources"
    },
    data: [{
      type: "pie",
      startAngle: 75,
      toolTipContent: "<b>{label}</b>: {y}%",
      showInLegend: "true",
      legendText: "{label}",
      indexLabelFontSize: 16,
      indexLabel: "{label} - {y}%",
      dataPoints: [
        { y: 18, label: "Direct" }
      ]
    }]})

  useEffect(() => {

    const token = service.getToken();
    const key = service.getKey();

    const setter = (i,p,d) => {
      data.current.data.dataPoints.push({y : p.count, label: d}); 
      // console.log(datas.current.data[i].dataPoints)
    }

    axios
    .get(`${API_ENDPOINT}/v1/gangster/dashboard/${JSON.stringify(props.startDate)}/${JSON.stringify(props.endDate)}`, { params: { access_token: token, x_key: key }  })
    .then( res => {
      console.log(res.data)
      res.data.dayData.map(d => {
        d.platform.map(p => {
          if(p.platform == 'b'){setter(0,p,"Bookipi");}
          if(p.platform == 'd'){setter(1,p,"Desktop");}
           if(p.platform == 's'){setter(2,p,"Super Simple");}
           if(p.platform == 'i'){setter(3,p,"Invoice Bee");}
           if(p.platform == 'e'){setter(4,p,"Bookipi Expense");}
        })
      })
    })
  }, []);

  return (
    <Card
      // {...rest}
      className={clsx(classes.root)}
    >
      <CardHeader
        action={<GenericMoreButton />}
        title=" 24 Hours Data"
      />
      <Divider />
      <CardContent className={classes.content}>
        {/* <PerfectScrollbar>
          <div className={classes.inner}>
          </div>
        </PerfectScrollbar> */}
        {console.log("this")}
        {console.log(data.current)}
         <CanvasJSChart
          options={data.current}
/> 
        {console.log("that")}
      </CardContent>
    </Card>
  );
}

export default UsageStats;
