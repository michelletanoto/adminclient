import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Divider,
  Table,
  TableBody,
  TableRow,
  TableCell,
  colors
} from '@material-ui/core';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  actions: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > * + *': {
      marginLeft: 0
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  }
}));

function PlatformInfo({ platform, className, ...rest }) {
  const classes = useStyles();
  const [openEdit, setOpenEdit] = useState(false);

  const handleEditOpen = () => {
    setOpenEdit(true);
  }

  const handleEditClose = () => {
    setOpenEdit(false);
  }

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Platform info" />
      <Divider />
      <CardContent className={classes.content}>
        <Table>
          <TableBody>
          <TableRow selected>
              <TableCell>Bookipi</TableCell>
              <TableCell>{platform.b ? "True" : "False"}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Bookipi Expense</TableCell>
              <TableCell>{platform.e ? "True" : "False"}</TableCell>
            </TableRow>
            <TableRow selected>
              <TableCell>Invoice Bee</TableCell>
              <TableCell>{platform.i ? "True" : "False"}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Gmail add-on</TableCell>
              <TableCell>{platform.g ? "True" : "False"}</TableCell>
            </TableRow>
            <TableRow selected>
              <TableCell>Web add-on</TableCell>
              <TableCell>{platform.w ? "True" : "False"}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Super Simple</TableCell>
              <TableCell>{platform.s ? "True" : "False"}</TableCell>
            </TableRow>
            <TableRow selected>
              <TableCell>Desktop</TableCell>
              <TableCell>{platform.d? "True" : "False"}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
}

PlatformInfo.propTypes = {
  className: PropTypes.string,
  platform: PropTypes.object.isRequired
};

export default PlatformInfo;
