import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import axios from 'src/utils/axios';
import CustomerInfo from './CustomerInfo';
import SendEmails from './SendEmails';
import OtherActions from './OtherActions';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';

const useStyles = makeStyles(() => ({
  root: {}
}));

const Summary = ({match}) => {
  const classes = useStyles();
  const [customer, setCustomer] = useState();
  const { id} = match.params;

  useEffect(() => {
    let mounted = true;

    const token = service.getToken()
    const key = service.getKey()
    const user = service.getUser()

    const fetchCustomer = () => {
      axios.get(`${API_ENDPOINT}/v1/customers/one/${id}`,  {params: { access_token: token, x_key: key} })
      .then(response => {
        if (mounted) {
          setCustomer(response.data);
          console.log(customer)
        }
      })
      .catch((error) => console.log(error));
    }

    fetchCustomer();

    return () => {
      mounted = false;
    };
  }, []);

  if (!customer) {
    return null;
  }

  return (
    <Grid
      // {...rest}
      className={clsx(classes.root)}
      container
      spacing={3}
    >
      <Grid
        item
        lg={12}
        md={6}
        xl={3}
        xs={12}
      >
        <CustomerInfo customer={customer} />
      </Grid>
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <SendEmails customer={customer} />
      </Grid>
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <OtherActions customer={customer} />
      </Grid>
    </Grid>
  );
}

// Summary.propTypes = {
//   className: PropTypes.string
// };

export default Summary;
