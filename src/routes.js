/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import AuthLayout from './layouts/Auth';
import ErrorLayout from './layouts/Error';
import DashboardLayout from './layouts/Dashboard';
import DashboardAnalyticsView from './views/DashboardAnalytics';
import DashboardDefaultView from './views/DashboardDefault';
import OverviewView from './views/Overview';

export default [
  {
    path: '/',
    exact: true,
    component: () => <Redirect to="/auth/login" />
  },
  {
    path: '/auth',
    component: AuthLayout,
    routes: [
      {
        path: '/auth/login',
        exact: true,
        component: lazy(() => import('src/views/Login'))
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      }
    ]
  },
  {
    path: '/errors',
    component: ErrorLayout,
    routes: [
      {
        path: '/errors/error-401',
        exact: true,
        component: lazy(() => import('src/views/Error401'))
      },
      {
        path: '/errors/error-404',
        exact: true,
        component: lazy(() => import('src/views/Error404'))
      },
      {
        path: '/errors/error-500',
        exact: true,
        component: lazy(() => import('src/views/Error500'))
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      }
    ]
  },
  {
    route: '*',
    component: DashboardLayout,
    routes: [
      {
        path: '/dashboards/analytics',
        exact: true,
        component: DashboardAnalyticsView
      },
      {
        path: '/dashboards/default',
        exact: true,
        component: DashboardDefaultView
      },
      {
        path: '/management/customers',
        exact: true,
        component: lazy(() => import('src/views/Customers'))
      },
      {
        path: '/management/customers/:id/',
        exact: true,
        component: lazy(() => import('src/views/CustomerDetails'))
      },
      {
        path: '/management/customers/:id/summary',
        exact: true,
        component: lazy(() => import('src/views/CustomerDetails/Summary'))
      },
      {
        path: '/management/company',
        exact: true,
        component: lazy(() => import('src/views/Company'))
      },
      {
        path: '/management/company/:id/',
        exact: true,
        component: lazy(() => import('src/views/CompanyDetails'))
      },
      {
        path: '/management/company/:id/summary',
        exact: true,
        component: lazy(() => import('src/views/CompanyDetails/Summary'))
      },
      {
        path: '/management/company/:id/:tab',
        exact: true,
        component: lazy(() => import('src/views/CompanyDetails'))
      },
      {
        path: '/management/users',
        exact: true,
        component: lazy(() => import('src/views/Users'))
      },
      {
        path: '/management/users/:id/',
        exact: true,
        component: lazy(() => import('src/views/UserDetails'))
      },
      {
        path: '/management/users/:id/summary',
        exact: true,
        component: lazy(() => import('src/views/UserDetails/Summary'))
      },
      {
        path: '/management/users/:id/:tab',
        exact: true,
        component: lazy(() => import('src/views/UserDetails'))
      },
      {
        path: '/overview',
        exact: true,
        component: OverviewView
      },
      {
        path: '/profile/:id',
        exact: true,
        component: lazy(() => import('src/views/Profile'))
      },
      {
        path: '/profile/:id/:tab',
        exact: true,
        component: lazy(() => import('src/views/Profile'))
      },
      {
        path: '/settings',
        exact: true,
        component: lazy(() => import('src/views/Settings'))
      },
      {
        path: '/settings/:tab',
        exact: true,
        component: lazy(() => import('src/views/Settings'))
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      }
    ]
  }
];
