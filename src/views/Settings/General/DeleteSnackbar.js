import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Snackbar, SnackbarContent, colors } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircleOutlined';

const useStyles = makeStyles((theme) => ({
  content: {
    backgroundColor: colors.red[600]
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  },
  icon: {
    marginRight: theme.spacing(2)
  }
}));

function DeleteSnackbar({ open, onClose }) {
  const classes = useStyles();

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}
      autoHideDuration={6000}
      onClose={onClose}
      open={open}
    >
      <SnackbarContent
        className={classes.content}
        message={(
          <span className={classes.message}>
            <CheckCircleIcon className={classes.icon} />
            Successfully deleted account!
          </span>
        )}
        variant="h6"
      />
    </Snackbar>
  );
}

DeleteSnackbar.propTypes = {
  onClose: PropTypes.func,
  open: PropTypes.bool
};

DeleteSnackbar.defaultProps = {
  open: true,
  onClose: () => {}
};

export default DeleteSnackbar