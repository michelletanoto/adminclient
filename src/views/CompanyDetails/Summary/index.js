import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import axios from 'src/utils/axios';
import CompanyInfo from './CompanyInfo';
import SendEmails from './SendEmails';
import OtherActions from './OtherActions';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';

const useStyles = makeStyles(() => ({
  root: {}
}));

const Summary = ({match}) => {
  const classes = useStyles();
  const [company, setCompany] = useState();
  const { id } = match.params;

  useEffect(() => {
    let mounted = true;

    const token = service.getToken()
    const key = service.getKey()
    const user = service.getUser()

    const fetchCompany = () => {
      axios.get(`${API_ENDPOINT}/v1/company/one/${id}`, {params: { access_token: token, x_key: key } })
      .then(response => {
        if (mounted) {
          setCompany(response.data);
          console.log(company)
        }
      })
      .catch((error) => console.log(error));
    }

    fetchCompany();

    return () => {
      mounted = false;
    };
  }, []);

  if (!company) {
    return null;
  }

  return (
    <Grid
      // {...rest}
      className={clsx(classes.root)}
      container
      spacing={3}
    >
      <Grid
        item
        lg={12}
        md={6}
        xl={3}
        xs={12}
      >
        <CompanyInfo company={company} />
      </Grid>
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <SendEmails company={company} />
      </Grid>
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <OtherActions company={company} />
      </Grid>
    </Grid>
  );
}

// Summary.propTypes = {
//   className: PropTypes.string
// };

export default Summary;
