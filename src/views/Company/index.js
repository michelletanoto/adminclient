import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Container } from '@material-ui/core';
import axios from 'axios';
import Page from 'src/components/Page';
import SearchBar from 'src/components/SearchBar';
import Header from '../Company/Header';
import Results from './Results';
import { API_ENDPOINT } from '../../config/config';
import service from '../../utils/token';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

function CompanyManagementList() {
  const classes = useStyles();
  const [company, setCompany] = useState([]);
  const [search, setSearch] = useState('');
  const [result, setResult] = useState([]);

const handleChange = (event) => {
  event.preventDefault();
  setSearch(event.target.value);
  console.log(search);
}

const handleSearch = (event) => { 

  const token = service.getToken();
  const key = service.getKey();
  const user = service.getUser()
  axios.get(`${API_ENDPOINT}/v1/company/${search}`, {params: { access_token: token, x_key: key} })
  .then((response) => {
    console.log(response.data)
    setResult(response.data)
  })
  .catch(error => console.log("error"))
};

  return (
    <Page
      className={classes.root}
      title="Company Management List"
    >
      <Container maxWidth={false}>
        <Header />
        <SearchBar
          // onFilter={handleFilter}
          onChange={handleChange}
          onSearch={handleSearch}
          placeholder="Search Email"
        />
        {company && (
          <Results
            className={classes.results}
            companies={result}
          />
        )}
      </Container>
    </Page>
  );
}

export default CompanyManagementList;
