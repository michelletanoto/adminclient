import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Divider,
  Table,
  TableBody,
  TableRow,
  TableCell,
  colors
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import LockOpenIcon from '@material-ui/icons/LockOpenOutlined';
import PersonIcon from '@material-ui/icons/PersonOutline';
import Label from 'src/components/Label';
import CompanyEditModal from './CompanyEditModal';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  actions: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > * + *': {
      marginLeft: 0
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  }
}));

function CompanyInfo({user , className, ...rest }) {
  const classes = useStyles();
  const [openEdit, setOpenEdit] = useState(false);
  const [company, setCompany] = useState({});

  const handleEditOpen = () => {
    setOpenEdit(true);
  }

  const handleEditClose = () => {
    setOpenEdit(false);
  }

  useEffect(() => {
    let mounted = true;

    const token = service.getToken()
    const key = service.getKey()
    const user = service.getUser()

    const fetchCompany = () => {
      axios.get(`${API_ENDPOINT}/v1/company/one/${user.dci}`, {params: { access_token: token, x_key: key } })
      .then(response => {
        if (mounted) {
          setCompany(response.data);
        }
      })
      .catch((error) => console.log(error));
    }

    fetchCompany();

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Company info" />
      <Divider />
      <CardContent className={classes.content}>
        <Table>
          <TableBody>
          <TableRow selected>
              <TableCell>Name</TableCell>
              <TableCell>{company.c}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Email</TableCell>
              <TableCell>
                {company.de}
                <div>
                  <Label
                    color={
                      company.vf ? colors.green[600] : colors.orange[600]
                    }
                  >
            {company.vf
                      ? 'Email verified'
                      : 'Email not verified'}
                  </Label>
                </div>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Country</TableCell>
              <TableCell>{company.co}</TableCell>
            </TableRow>
            <TableRow selected>
              <TableCell>State</TableCell>
              <TableCell>{company.st}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </CardContent>
      <CardActions className={classes.actions}>
        <Button onClick={handleEditOpen}>
          <EditIcon className={classes.buttonIcon} />
          Edit
        </Button>
      </CardActions>
      <CompanyEditModal 
        company={company}
        onClose={handleEditClose}
        open={openEdit}
      />
    </Card>
  );
}

CompanyInfo.propTypes = {
  className: PropTypes.string,
  user: PropTypes.object.isRequired
};

export default CompanyInfo;
