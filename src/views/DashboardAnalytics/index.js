import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Container, Grid } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import { Redirect } from 'react-router-dom';
import service from '../../utils/token';
import RegisterStats from './RegisterStats';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function DashboardAnalytics() {
  const classes = useStyles();

  if(!service.getUser()){
    return <Redirect to= "/errors/error-401" />;
  }
  
  return (
    <Page
      className={classes.root}
      title="Analytics Dashboard"
    >
      <Container maxWidth={false}>
        <Header />
        <br />
        {/* <Grid
          container
          spacing={3}
        > */}
           {/* <Grid
            item
            lg={8}
            xs={12}
          >
          </Grid>
          <Grid
            item
            lg={4}
            xs={12}
          >
          </Grid>
          <Grid
            item
            lg={8}
            xs={12}
          >
          </Grid>
          <Grid
            item
            lg={4}
            xs={12}
          >
          </Grid> */} 
        {/* </Grid>  */}
      </Container>
    </Page>
  );
}

export default DashboardAnalytics;
