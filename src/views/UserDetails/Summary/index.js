import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import axios from 'src/utils/axios';
import UserInfo from './UserInfo';
import CompanyInfo from './CompanyInfo';
import UserPlatforms from './UserPlatforms';
import SendEmails from './SendEmails';
import OtherActions from './OtherActions';
import { API_ENDPOINT } from '../../../config/config';
import service from '../../../utils/token';

const useStyles = makeStyles(() => ({
  root: {}
}));

const Summary = ({match}) => {
  const classes = useStyles();
  const [user, setUser] = useState();
  const [platform, setPlatform] = useState({});
  const { id} = match.params;


  useEffect(() => {
    let mounted = true;

    const token = service.getToken()
    const key = service.getKey()
    const user = service.getUser()

    const fetchPlatform = () => {
      axios.get(`${API_ENDPOINT}/v1/accessLog/${id}`, {params: { access_token: token, x_key: key } })
      .then(response => {
        if (mounted) {
          setPlatform(response.data.ip);
        }
      })
      .catch((error) => console.log(error));
    }

    fetchPlatform();

    return () => {
      mounted = false;
    };
  }, []);

  useEffect(() => {
    let mounted = true;

    const token = service.getToken()
    const key = service.getKey()
    const user = service.getUser()

    const fetchUser = () => {
      axios.get(`${API_ENDPOINT}/v1/users/one/${id}`,  {params: { access_token: token, x_key: key} })
      .then(response => {
        if (mounted) {
          setUser(response.data);
          console.log(user)
        }
      })
      .catch((error) => console.log(error));
    }

    fetchUser();

    return () => {
      mounted = false;
    };
  }, []);

  if (!user) {
    return null;
  }

  return (
    <Grid
      // {...rest}
      className={clsx(classes.root)}
      container
      spacing={3}
    >
      <Grid
        item
        lg={12}
        md={6}
        xl={3}
        xs={12}
      >
        <UserInfo user={user} />
      </Grid>
      <Grid
        item
        lg={12}
        md={6}
        xl={3}
        xs={12}
      >
        <UserPlatforms platform={platform} />
      </Grid>
      <Grid
        item
        lg={12}
        md={6}
        xl={3}
        xs={12}
      >
        <CompanyInfo user={user} />
      </Grid>
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <SendEmails user={user} />
      </Grid>
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <OtherActions user={user} />
      </Grid>
    </Grid>
  );
}

// Summary.propTypes = {
//   className: PropTypes.string
// };

export default Summary;
