import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Divider,
  Table,
  TableBody,
  TableRow,
  TableCell,
  colors
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import LockOpenIcon from '@material-ui/icons/LockOpenOutlined';
import PersonIcon from '@material-ui/icons/PersonOutline';
import Label from 'src/components/Label';
import CompanyEditModal from './CompanyEditModal';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  actions: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > * + *': {
      marginLeft: 0
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  }
}));

function CompanyInfo({ company, className, ...rest }) {
  const classes = useStyles();
  const [openEdit, setOpenEdit] = useState(false);

  const handleEditOpen = () => {
    setOpenEdit(true);
  }

  const handleEditClose = () => {
    setOpenEdit(false);
  }

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Company info" />
      <Divider />
      <CardContent className={classes.content}>
        <Table>
          <TableBody>
          <TableRow selected>
              <TableCell>Company Name</TableCell>
              <TableCell>{company.c}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Company Email</TableCell>
              <TableCell>
                {company.e}
                <div>
                  <Label
                    color={
                      company.vf ? colors.green[600] : colors.orange[600]
                    }
                  >
                    {company.vf
                      ? 'Email verified'
                      : 'Email not verified'}
                  </Label>
                </div>
              </TableCell>
            </TableRow>
            <TableRow selected> 
              <TableCell>Location</TableCell>
              <TableCell>{company.co}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>City</TableCell>
              <TableCell>{company.tz}</TableCell>
            </TableRow>
            <TableRow selected>
              <TableCell>State</TableCell>
              <TableCell>{company.st}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </CardContent>
      <CardActions className={classes.actions}>
        <Button onClick={handleEditOpen}>
          <EditIcon className={classes.buttonIcon} />
          Edit
        </Button>
        <Button>
          <LockOpenIcon className={classes.buttonIcon} />
          Reset &amp; Send Password
        </Button>
        <Button>
          <PersonIcon className={classes.buttonIcon} />
          Login as Company
        </Button>
      </CardActions>
      <CompanyEditModal
        company={company}
        onClose={handleEditClose}
        open={openEdit}
      />
    </Card>
  );
}

CompanyInfo.propTypes = {
  className: PropTypes.string,
  company: PropTypes.object.isRequired
};

export default CompanyInfo;
